data LinkedList a = Empty | Cons a (LinkedList a)

cons :: a -> LinkedList a -> LinkedList a
cons elem list = Cons elem list

first :: LinkedList a -> Maybe a
first Empty = Nothing
first (Cons first _) = Just first

main :: IO ()
main =
  let
    a = Empty
  in print "asdf"
