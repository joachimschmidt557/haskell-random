data Tower = First | Middle | Last deriving Show

data Instruction = Move Tower Tower deriving Show

-- show :: Instruction -> String
-- show Move from to = "Moving " ++ show from ++ " to " ++ show to

solve :: Tower -> Tower -> Tower -> Integer -> [Instruction]
solve _ _ _ 0 = []
solve from over to n = solve from to over (n - 1) ++ [Move from to] ++ solve over from to (n - 1)

main :: IO ()
main = let
  solution = solve First Middle Last 4
  in
  print solution
